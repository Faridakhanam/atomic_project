<?php
    namespace src\Bitm\SEIP106174\book_title;
    
     class Book {
        public $id;
        public $book_title;
        public $created;
        public $modified;
        public $created_by;
        public $modified_by;
        public $deleted_at;
        
        public function __construct ($book_title=null){
            $this->book_title=$book_title;             
     }
        public function index(){
            echo " I am listing data";
        }
         public function create(){
            echo " I am create form ";
        }
         public function store(){
            echo " I am storing data";
        }
         public function edit(){
            echo " I am editing data";
        }
         public function update(){
            echo " I am updating data";
        }
         public function delete(){
            echo " I am delete data";
        }
        
        }

?>
<?php

    namespace src\Bitm\SEIP106174\texearea;
    
     class Summary {
        public $id;
        public $summary_of_organization;
        public $created;
        public $modified;
        public $created_by;
        public $modified_by;
        public $deleted_at;
        
        public function __construct ($summary_of_organization=null){
            $this->summary_of_organization=$summary_of_organization;             
     }
        public function index(){
            echo " I am listing data";
        }
         public function create(){
            echo " I am create form ";
        }
         public function store(){
            echo " I am storing data";
        }
         public function edit(){
            echo " I am editing data";
        }
         public function update(){
            echo " I am updating data";
        }
         public function delete(){
            echo " I am delete data";
        }
        
        }

?>